export const SET_LOCAL = 'Shortly/SET_LOCAL';
export const SET_THEME = 'Shortly/SET_THEME';
export const GET_SHORT_URL = 'Shortly/GET_SHORT_URL';
export const SET_SHORT_URL = 'Shortly/SET_SHORT_URL';
export const SET_SHORT_URL_LOADING = 'Shortly/SET_SHORT_URL_LOADING';
export const SET_SHORT_URL_ERROR = 'Shortly/SET_SHORT_URL_ERROR';
export const DELETE_SHORT_URL = 'Shortly/DELETE_SHORT_URL';
