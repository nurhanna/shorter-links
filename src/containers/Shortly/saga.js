import { takeLatest, call, put, select } from 'redux-saga/effects';

import { getShortUrl } from '@domain/api';
import { setShortUrl, setShortUrlLoading, setShortUrlError } from '@containers/Shortly/actions';

import { GET_SHORT_URL } from '@containers/Shortly/constants';

import { selectShortUrl } from '@containers/Shortly/selectors';

export function* doGetShortUrl({ url }) {
  yield put(setShortUrlLoading(true));
  try {
    const data = yield select(selectShortUrl);
    if (url !== '' && data.map((item) => item.original_link).find((link) => link.includes(url))) {
      yield put(setShortUrlError({ errorCode: 11 }));

      yield put(setShortUrlLoading(false));

      return -1;
    }

    const shortUrl = yield call(getShortUrl, url);
    if (shortUrl?.result) {
      yield put(setShortUrlError(null));
      yield put(setShortUrl(shortUrl.result));
    }
  } catch (err) {
    const { response } = err;
    console.log(response);

    const error = response?.data?.error;
    const errorCode = response?.data?.error_code;
    yield put(setShortUrlError({ error, errorCode }));
    // console.log(errorCode, error);
  }
  yield put(setShortUrlLoading(false));
}

export default function* appSaga() {
  yield takeLatest(GET_SHORT_URL, doGetShortUrl);
}
