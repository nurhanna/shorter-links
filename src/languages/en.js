export default {
  app_greeting: 'Hi from Web!',
  app_title_header: 'Vite + React',
  app_not_found: 'Page not found',
  app_lang_id: 'Indonesian',
  app_lang_en: 'English',

  // header
  shortly_logo: 'Shortly',
  shortly_first_menu: 'Features',
  shortly_second_menu: 'Pricing',
  shortly_third_menu: 'Resources',
  shortly_login: 'Login',
  shortly_signup: 'Sign Up',

  //
  home_title: 'More than just shorter links',
  home_subtitle: "Build your brand's recognition and get detailed insights on how your links are performing.",
  home_get_started: 'Get started',

  //
  input_placeholder: 'Shorten a link here...',
  shorten_it: 'Shorten it!',

  // section 2
  statistic_title: 'Advanced Statistics',
  statistic_subtitle: 'track how your links are performing across the web with our advanced statistics dashboard.',

  // card
  card_title1: 'Brand Recognition',
  card_subtitle1:
    "Boost your brand recognition with each click. Generic links don't mean a thing. Branded links help instil confidence in your content",
  card_title2: 'Detailed Records',
  card_subtitle2:
    'Gain insights into who is clicking your links. Knowing when and where people engage with your content helps inform better decisions.',
  card_title3: 'Fully Customizable',
  card_subtitle3: 'Improve brand awareness and content discoverability through customizable links.',

  // section 3
  boost_links: 'Boost your links today',

  // footer
  company: 'Company',
  link_shortening: 'Link Shortening',
  branded_links: 'Branded Links',
  analytics: 'Analytics',
  blog: 'Blog',
  developers: 'Developers',
  support: 'Support',
  about: 'About',
  our_team: 'Our Team',
  careers: 'Careers',
  contact: 'Contact',

  // error
  err1: 'Please add a link',
  err2: 'Please insert a valid url',
  err3: 'Rate limit reached. Wait a second and try again',
  err4: 'IP-Address has been blocked because of violating our terms of service',
  err5: 'shrtcode code (slug) already taken/in use',
  err6: 'Unknown error',
  err7: 'No code specified',
  err8: 'Invalid code submitted (code not found/there is no such short-link)',
  err9: 'Missing required parameters',
  err10: 'disallowed Link',
  err11: 'Duplicat Link',

  copy: 'Copy',
  copied: 'Copied!',
  languange: 'Language',
};
