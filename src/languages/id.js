import Shortly from '@pages/Shortly';

export default {
  app_greeting: 'Hai dari Web!',
  app_title_header: 'Vite + React',
  app_not_found: 'Halaman tidak ditemukan',
  app_lang_id: 'Bahasa Indonesia',
  app_lang_en: 'Bahasa Inggris',

  // header
  shortly_logo: 'Shortly',
  shortly_first_menu: 'Fitur',
  shortly_second_menu: 'Harga',
  shortly_third_menu: 'Referensi',
  shortly_login: 'Masuk',
  shortly_signup: 'Daftar',

  //
  home_title: 'Lebih dari sekedar tautan yang dipersingkat',
  home_subtitle: 'Bangun reputasi bisnis Anda dan dapatkan wawasan mendetail tentang kinerja tautan Anda.',
  home_get_started: 'Mulai',

  //
  input_placeholder: 'Persingkat tautan di sini...',
  shorten_it: 'Persingkat!',

  // section 2
  statistic_title: 'Statistik Tingkat Lanjut',
  statistic_subtitle: 'Lacak kinerja tautan Anda di seluruh web dengan dasbor statistik lanjutan kami.',

  // card
  card_title1: 'Pengenalan Bisnis',
  card_subtitle1:
    'Tingkatkan pengenalan bisnis Anda dengan setiap klik. Tautan umum tidak berarti apa-apa. Tautan bermerek membantu menanamkan kepercayaan pada konten Anda',
  card_title2: 'Riwayat Detail',
  card_subtitle2:
    'Dapatkan informasi tentang siapa yang mengklik tautan Anda. Mengetahui kapan dan di mana orang berinteraksi dengan konten Anda akan membantu Anda mengambil keputusan yang lebih baik.',
  card_title3: 'Disesuaikan Sepenuhnya',
  card_subtitle3: 'Maksimalkan pengenalan brand dan penemuan konten melalui tautan yang dapat disesuaikan.',

  // section 3
  boost_links: 'Tingkatkan tautan Anda sekarang juga',

  // footer
  company: 'Perusahaan',
  link_shortening: 'Tautan yang dipersingkat',
  branded_links: 'Tautan Bermerek',
  analytics: 'Analitik',
  blog: 'Blog',
  developers: 'Pengembang',
  support: 'Bantuan',
  about: 'Tentang',
  our_team: 'Tim kami',
  careers: 'Karir',
  contact: 'Kontak',

  // error
  err1: 'Silakan tambahkan tautan',
  err2: 'Harap masukkan url yang valid',
  err3: 'Batas pemakaian tercapai. Tunggu sebentar dan coba lagi',
  err4: 'Alamat IP telah diblokir karena melanggar ketentuan layanan kami',
  err5: 'kode shrtcode ( slug ) sudah diambil/digunakan',
  err6: 'Kesalahan tidak diketahui',
  err7: 'Tidak ada kode yang ditunjukkan',
  err8: 'Kode yang dikirimkan tidak valid (kode tidak ditemukan/tidak ada tautan pendek)',
  err9: 'Parameter yang dibutuhkan tidak ada',
  err10: 'Tautan tidak diizinkan',
  err11: 'Link Duplikat',

  copy: 'Salin',
  copied: 'Tersalin!',

  languange: 'Bahasa',
};
