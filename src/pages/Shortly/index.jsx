import classes from './style.module.scss';
import { FormattedMessage } from 'react-intl';
import { injectIntl } from 'react-intl';

import { connect, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import logo from '@static/images/logo.svg';
import people from '@static/images/working.svg';
import brandRecognition from '@static/images/icon-brand-recognition.svg';
import detailedRecords from '@static/images/icon-detailed-records.svg';
import fullyCustomizable from '@static/images/icon-fully-customizable.svg';
import instagram from '@static/images/icon-instagram.svg';
import facebook from '@static/images/icon-facebook.svg';
import twitter from '@static/images/icon-twitter.svg';
import pinterest from '@static/images/icon-pinterest.svg';
import LinearProgress from '@mui/material/LinearProgress';
// import Navbar from '@components/Shortly/Navbar';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import FlagId from '@static/images/flags/id.png';
import FlagEn from '@static/images/flags/en.png';

// selector
import {
  selectShortUrl,
  selectShortUrlLoading,
  selectShortUrlError,
  selectLocale,
} from '@containers/Shortly/selectors';

// action
import { getShortUrl, setLocale, deleteShortUrl } from '@containers/Shortly/actions';

const Shortly = ({ shortUrl, shortError, shortLoading, locale, intl }) => {
  useEffect(() => {}, [shortLoading]);
  const dispatch = useDispatch();

  // handle language
  const [menuPosition, setMenuPosition] = useState(null);
  const open = Boolean(menuPosition);
  const handleClick = (event) => {
    setMenuPosition(event.currentTarget);
  };
  const handleClose = () => {
    setMenuPosition(null);
  };
  const onSelectLang = (lang) => {
    if (lang !== locale) {
      dispatch(setLocale(lang));
    }
    handleClose();
  };

  // handle link input
  const [link, setLink] = useState('');
  const handleInput = () => {
    const val = document.getElementById('myInput').value;
    setLink(val);
  };

  const getUrl = () => {
    dispatch(getShortUrl(link));
    setLink('');
    // shortError ? handleMessageError(shortError.errorCode) : null;
  };

  // handle error
  const [message, setMessage] = useState('');
  const handleMessageError = (code) => {
    switch (code) {
      case 1:
        setMessage(<FormattedMessage id="err1" />);
        break;
      case 2:
        setMessage(<FormattedMessage id="err2" />);
        break;
      case 3:
        setMessage(<FormattedMessage id="err3" />);
        break;
      case 4:
        setMessage(<FormattedMessage id="err4" />);
        break;
      case 5:
        setMessage(<FormattedMessage id="err5" />);
        break;
      case 6:
        setMessage(<FormattedMessage id="err6" />);
        break;
      case 7:
        setMessage(<FormattedMessage id="err7" />);
        break;
      case 8:
        setMessage(<FormattedMessage id="err8" />);
        break;
      case 9:
        setMessage(<FormattedMessage id="err9" />);
        break;
      case 10:
        setMessage(<FormattedMessage id="err10" />);
        break;
      case 11:
        setMessage(<FormattedMessage id="err11" />);
        break;
      default:
        setMessage('');
        break;
    }
  };
  useEffect(() => {
    shortError ? handleMessageError(shortError.errorCode) : null;
  }, [shortError]);

  // copy
  const [copy, setCopy] = useState(null);

  // handle navigation for mobile
  const [display, setDisplay] = useState(false);
  const displayMenuMobile = () => {
    display === true ? setDisplay(false) : setDisplay(true);
  };

  // handle delete
  const handleDelete = (code) => {
    dispatch(deleteShortUrl(code));
  };

  return (
    <div className={classes.containerBase}>
      <div className={classes.container}>
        {/* section 1 */}
        <div className={classes.sectionOne}>
          <div className={classes.dekstop}>
            {/* section1-header */}
            <div className={classes.header}>
              <div className={classes.logo}>
                <img src={logo} />
              </div>
              <div className={classes.left}>
                <div>
                  <FormattedMessage id="shortly_first_menu" />
                </div>
                <div>
                  <FormattedMessage id="shortly_second_menu" />
                </div>
                <div>
                  <FormattedMessage id="shortly_third_menu" />
                </div>
              </div>
              <div className={classes.right}>
                <div className={classes.login}>
                  <FormattedMessage id="shortly_login" />
                </div>
                <div className={classes.signup}>
                  <FormattedMessage id="shortly_signup" />
                </div>
                <div className={classes.locale}>
                  <div className={classes.toggle} onClick={handleClick}>
                    <Avatar className={classes.avatar} src={locale === 'id' ? FlagId : FlagEn} />
                    <div className={classes.lang}>{locale}</div>
                    <ExpandMoreIcon />
                  </div>
                  <Menu open={open} anchorEl={menuPosition} onClose={handleClose}>
                    <MenuItem onClick={() => onSelectLang('id')} selected={locale === 'id'}>
                      <div className={classes.menu}>
                        <Avatar className={classes.menuAvatar} src={FlagId} />
                        <div className={classes.menuLang}>
                          <FormattedMessage id="app_lang_id" />
                        </div>
                      </div>
                    </MenuItem>
                    <MenuItem onClick={() => onSelectLang('en')} selected={locale === 'en'}>
                      <div className={classes.menu}>
                        <Avatar className={classes.menuAvatar} src={FlagEn} />
                        <div className={classes.menuLang}>
                          <FormattedMessage id="app_lang_en" />
                        </div>
                      </div>
                    </MenuItem>
                  </Menu>
                </div>
              </div>
            </div>
          </div>
          {/* mobile */}
          <div className={classes.mobile}>
            <div className={classes.menuMobile}>
              <div className={locale.mobileLeft}>
                <img src={logo} />
              </div>
              <div className={classes.mobileRight}>
                <div className={classes.localeMobile}>
                  <div className={classes.toggle} onClick={handleClick}>
                    <Avatar className={classes.avatar} src={locale === 'id' ? FlagId : FlagEn} />
                    {/* <div className={classes.lang}>{locale}</div> */}
                    <ExpandMoreIcon />
                  </div>
                  <Menu open={open} anchorEl={menuPosition} onClose={handleClose}>
                    <MenuItem onClick={() => onSelectLang('id')} selected={locale === 'id'}>
                      <div className={classes.menu}>
                        <Avatar className={classes.menuAvatar} src={FlagId} />
                        <div className={classes.menuLang}>
                          <FormattedMessage id="app_lang_id" />
                        </div>
                      </div>
                    </MenuItem>
                    <MenuItem onClick={() => onSelectLang('en')} selected={locale === 'en'}>
                      <div className={classes.menu}>
                        <Avatar className={classes.menuAvatar} src={FlagEn} />
                        <div className={classes.menuLang}>
                          <FormattedMessage id="app_lang_en" />
                        </div>
                      </div>
                    </MenuItem>
                  </Menu>
                </div>

                <button onClick={displayMenuMobile} className={classes.mobileToogle}>
                  <i className="fa fa-bars"></i>
                </button>
              </div>
            </div>

            <div className={classes.mobileContainer}>
              <div className={classes.topnav} style={display === false ? { display: 'none' } : null}>
                <div id="myLinks">
                  <p>
                    <FormattedMessage id="shortly_first_menu" />
                  </p>
                  <p>
                    <FormattedMessage id="shortly_second_menu" />
                  </p>
                  <p>
                    <FormattedMessage id="shortly_third_menu" />
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* section 1-home */}
          <div className={classes.home}>
            <div className={classes.left}>
              <div className={classes.title}>
                <FormattedMessage id="home_title" />
              </div>
              <div className={classes.subtitle}>
                <FormattedMessage id="home_subtitle" />
              </div>
              <div className={classes.getStarted}>
                <button>
                  <FormattedMessage id="home_get_started" />
                </button>
              </div>
            </div>
            <div className={classes.right}>
              <img src={people}></img>
            </div>
          </div>
        </div>

        {/* section 2 */}
        <div className={classes.sectionTwo}>
          {/* section 2-input */}
          <div className={classes.containerInput}>
            <input
              type="text"
              placeholder={intl.formatMessage({ id: 'input_placeholder' })}
              id="myInput"
              value={link}
              onChange={handleInput}
            />
            <button onClick={getUrl}>
              <FormattedMessage id="shorten_it" />
            </button>
          </div>

          <div className={classes.errorMessage}>
            {message && message}
            {shortLoading && <LinearProgress color="secondary" />}
          </div>
          {/* {message ? <div className={classes.errorMessage}>{message}</div> : null} */}

          {/* section 2 - list short link */}
          <div className={classes.dataContainer}>
            {shortUrl.length > 0
              ? shortUrl?.slice(-5).map((item, index) => (
                  <div className={classes.dataShort} key={index}>
                    <div className={classes.original}>{item.original_link}</div>
                    <div className={classes.short}>{item.short_link}</div>
                    <div className={classes.copy}>
                      <button
                        className={copy === item.code ? classes.copied : null}
                        onClick={() => {
                          navigator.clipboard.writeText(item.short_link);
                          setCopy(item.code);
                        }}
                      >
                        {copy === item.code ? <FormattedMessage id="copied" /> : <FormattedMessage id="copy" />}
                      </button>
                      <button className={classes.delete} onClick={() => handleDelete(item.code)}>
                        Delete
                      </button>
                    </div>
                  </div>
                ))
              : null}
          </div>
          {/* statistic */}
          <div className={classes.statistics}>
            <div className={classes.title}>
              <FormattedMessage id="statistic_title" />
            </div>
            <div className={classes.subtitle}>
              <FormattedMessage id="statistic_subtitle" />
            </div>
          </div>
          {/* Card */}
          <div className={classes.cardContainer}>
            <hr className={classes.line}></hr>
            <div className={classes.card1}>
              <img src={brandRecognition}></img>
              <div className={classes.title}>
                <FormattedMessage id="card_title1" />
              </div>
              <div className={classes.subtitle}>
                <FormattedMessage id="card_subtitle1" />
              </div>
            </div>
            <div className={classes.card2}>
              <img src={detailedRecords}></img>
              <div className={classes.title}>
                <FormattedMessage id="card_title2" />
              </div>
              <div className={classes.subtitle}>
                <FormattedMessage id="card_subtitle2" />
              </div>
            </div>
            <div className={classes.card3}>
              <img src={fullyCustomizable}></img>
              <div className={classes.title}>
                <FormattedMessage id="card_title3" />
              </div>
              <div className={classes.subtitle}>
                <FormattedMessage id="card_subtitle3" />
              </div>
            </div>
          </div>
        </div>
        {/* section 3 */}
        <div className={classes.sectionThree}>
          <div>
            <FormattedMessage id="boost_links" />
          </div>
          <button>
            <FormattedMessage id="home_get_started" />
          </button>
        </div>
        {/* section 4 - footer */}
        <div className={classes.footer}>
          <div className={classes.footerLogo}>Shortly</div>
          <div className={classes.footerMenu}>
            <div className={classes.footerGroupMenu}>
              <div className={classes.mainTitle}>
                <FormattedMessage id="shortly_first_menu" />
              </div>
              <div>
                <FormattedMessage id="link_shortening" />
              </div>
              <div>
                <FormattedMessage id="branded_links" />
              </div>
              <div>
                <FormattedMessage id="analytics" />
              </div>
            </div>

            <div className={classes.footerGroupMenu}>
              <div className={classes.mainTitle}>
                <FormattedMessage id="shortly_third_menu" />
              </div>
              <div>
                <FormattedMessage id="blog" />
              </div>
              <div>
                <FormattedMessage id="developers" />
              </div>
              <div>
                <FormattedMessage id="support" />
              </div>
            </div>

            <div className={classes.footerGroupMenu}>
              <div className={classes.mainTitle}>
                <FormattedMessage id="company" />
              </div>
              <div>
                <FormattedMessage id="about" />
              </div>
              <div>
                <FormattedMessage id="our_team" />
              </div>
              <div>
                <FormattedMessage id="careers" />
              </div>
              <div>
                <FormattedMessage id="contact" />
              </div>
            </div>

            <div className={classes.icon}>
              <div>
                <img src={facebook} />
              </div>
              <div>
                <img src={twitter} />
              </div>
              <div>
                <img src={pinterest} />
              </div>
              <div>
                <img src={instagram} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  shortUrl: selectShortUrl,
  shortLoading: selectShortUrlLoading,
  shortError: selectShortUrlError,
  locale: selectLocale,
});

Shortly.propTypes = {
  shortUrl: PropTypes.array,
  shortLoading: PropTypes.bool,
  shortError: PropTypes.object,
  locale: PropTypes.string,
  intl: PropTypes.object,
};
export default injectIntl(connect(mapStateToProps)(Shortly));
