import { all } from 'redux-saga/effects';

import appSaga from '@containers/Shortly/saga';

export default function* rootSaga() {
  yield all([appSaga()]);
}
