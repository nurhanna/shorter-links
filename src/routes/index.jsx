import MainLayout from '@layouts/MainLayout';

import Home from '@pages/Home';
import NotFound from '@pages/NotFound';
import Shortly from '@pages/Shortly';

const routes = [
  {
    path: '/',
    name: 'Shortly',
    component: Shortly,
  },

  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;
